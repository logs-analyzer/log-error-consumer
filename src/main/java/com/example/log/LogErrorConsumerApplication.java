package com.example.log;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogErrorConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogErrorConsumerApplication.class, args);
    }
}
