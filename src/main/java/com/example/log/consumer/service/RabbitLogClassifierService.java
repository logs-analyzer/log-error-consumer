package com.example.log.consumer.service;

import com.example.log.consumer.model.Log;
import com.example.log.consumer.model.LogLevel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@AllArgsConstructor
public class RabbitLogClassifierService implements LogClassifierService {

    private final QueueManager queueManager;
    private final RabbitTemplate rabbitTemplate;
    private final String exchangeName;

    @Override
    public void classify(Message logMessage) {

        var logText = new String(logMessage.getBody(), StandardCharsets.UTF_8);
        log.info("Message received from logs queue -> {}", logText);

        var queueName = extractQueueName(logText);

        if (queueName != null) {
            addToQueue(queueName, new Log(logText, LogLevel.INFO));
        }
    }

    private void addToQueue(String queueName, Log logMes) {
        queueManager.createQueueIfNotExists(queueName, queueName);
        rabbitTemplate.convertAndSend(exchangeName, queueName, logMes);
    }

    private String extractQueueName(String logText) {
        List<String> allMatches = new ArrayList<String>();

        String fullMessagePattern = ".+?Candidate from [^.\\s]* with name “[^“]*” " +
                "changed status to “[^“]*” on vacancy “[^“]*”*";
        String vacancyPattern = "(?<=from )[^.\\s]*(?= with)|(?<=name )“[^“]*”(?= changed)|" +
                "(?<=to )“[^“]*”(?= on)|(?<=vacancy )“[^“]*”";

        Matcher fullMessageMatcher = Pattern.compile(fullMessagePattern).matcher(logText);

        if (fullMessageMatcher.matches()) {
            Matcher vacancyMatcher = Pattern.compile(vacancyPattern).matcher(logText);

            while (vacancyMatcher.find()) {
                allMatches.add(vacancyMatcher.group().replace("“", "").replace("”", ""));
            }

            return MessageFormat.format("{0}-{1}-candidate", allMatches.get(0), allMatches.get(2));
        }

        return null;
    }
}