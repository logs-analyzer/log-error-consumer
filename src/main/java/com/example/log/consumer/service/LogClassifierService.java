package com.example.log.consumer.service;

import org.springframework.amqp.core.Message;

public interface LogClassifierService {

    void classify(Message logMessage);
}
