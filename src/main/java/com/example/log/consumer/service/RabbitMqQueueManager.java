package com.example.log.consumer.service;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.*;

import java.util.Map;

@RequiredArgsConstructor
public class RabbitMqQueueManager implements QueueManager {

    private final DirectExchange exchange;
    private final AmqpAdmin amqpAdmin;

    private final Map<String, Object> queueParametersMap =
            Map.of("x-message-ttl", 3600000, "x-queue-mode", "lazy");

    @Override
    public void createQueueIfNotExists(String queueName, String routingKey) {

        if (amqpAdmin.getQueueInfo(queueName) == null) {
            Queue newQueue = new Queue(queueName, false, false, true, queueParametersMap);
            Binding binding = BindingBuilder.bind(newQueue).to(exchange).with(routingKey);
            amqpAdmin.declareQueue(newQueue);
            amqpAdmin.declareBinding(binding);
        }
    }
}
