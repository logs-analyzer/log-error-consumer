package com.example.log.consumer.service;

public interface QueueManager {

    void createQueueIfNotExists(String queueName, String routingKey);
}
