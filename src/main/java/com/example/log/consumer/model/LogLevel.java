package com.example.log.consumer.model;

import java.util.List;
import java.util.Random;

public enum LogLevel {

    ERROR, INFO, DEBUG, WARN;

    private static final List<LogLevel> VALUES = List.of(values());
    private static final int SIZE = VALUES.size();
    private static final Random RANDOM = new Random();

    public static LogLevel randomLogLevel() {
        return VALUES.get(RANDOM.nextInt(SIZE));
    }
}
