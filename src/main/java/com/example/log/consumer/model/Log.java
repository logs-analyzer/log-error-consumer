package com.example.log.consumer.model;

public record Log(String message, LogLevel level) {}

