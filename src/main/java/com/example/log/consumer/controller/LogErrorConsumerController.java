package com.example.log.consumer.controller;

import com.example.log.consumer.model.Log;
import com.example.log.consumer.service.LogClassifierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Slf4j
@RestController
public class LogErrorConsumerController {

    private LogClassifierService classifier;

    public LogErrorConsumerController(LogClassifierService service){
        this.classifier = service;
    }

    @RabbitListener(queues = "queue.logs")
    private void logsListener(Message logMessage) {
        classifier.classify(logMessage);
    }
}
