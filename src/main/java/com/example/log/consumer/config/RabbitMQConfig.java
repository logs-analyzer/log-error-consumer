package com.example.log.consumer.config;

import com.example.log.consumer.service.LogClassifierService;
import com.example.log.consumer.service.QueueManager;
import com.example.log.consumer.service.RabbitLogClassifierService;
import com.example.log.consumer.service.RabbitMqQueueManager;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class RabbitMQConfig {

    private static final String LOGS_QUEUE = "queue.logs";
    private static final String ROUTING_LOGS = "recruiting.logs";
    private static final Map<String, Object> quorumParameters = Map.of("x-queue-type", "quorum");

    @Bean
    QueueManager queueManager(AmqpAdmin amqpAdmin) {
        return new RabbitMqQueueManager(exchange(), amqpAdmin);
    }

    @Bean
    LogClassifierService classifier(RabbitTemplate rabbitTemplate, AmqpAdmin amqpAdmin) {
        return new RabbitLogClassifierService(queueManager(amqpAdmin), rabbitTemplate, exchange().getName());
    }

    @Bean
    Queue logsQueue() {
        return new Queue(LOGS_QUEUE, true, false, false, quorumParameters);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange("amq.direct");
    }

    @Bean
    Binding bindingError(Queue logsQueue, DirectExchange exchange) {
        return BindingBuilder.bind(logsQueue)
                .to(exchange)
                .with(ROUTING_LOGS);
    }

    @Bean
    MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    AmqpAdmin amqpAdmin(ConnectionFactory factory) {
        return new RabbitAdmin(factory);
    }

    @Bean
    RabbitTemplate rabbitTemplate(ConnectionFactory factory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(factory);
        rabbitTemplate.setMessageConverter(messageConverter());
        return rabbitTemplate;
    }
}

